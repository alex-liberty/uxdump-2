#include "mainwindow.h"
#include <QApplication>
#include <signal.h>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    struct sigaction term;
    term.sa_handler = MainWindow::termSignalHandler;
    sigemptyset(&term.sa_mask);
    term.sa_flags |= SA_RESTART;
    if (sigaction(SIGTERM, &term, 0) > 0) exit(-1);


    return a.exec();
}
