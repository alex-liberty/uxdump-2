#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QDir>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QCloseEvent>
#include <QProcess>
#include <QMessageBox>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QSocketNotifier>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    QString accessToken;
    QDateTime startTime, endTime;

    static void termSignalHandler(int unused);

protected:
    void closeEvent(QCloseEvent *event);
    void readConfig();

public slots:

     void handleSigTerm();

private slots:
//    void on_webView_loadStarted();

    void on_webView_urlChanged(const QUrl &arg1);

//    void on_pushButton_clicked();

    void replyFinished(QNetworkReply*);
 //   void saveOnExit();

  //  void on_pushButton_clicked();

    void on_pushButton_reload_clicked();

    void on_pushButton_quit_clicked();

    void on_pushButton_extract_clicked();

private:
    Ui::MainWindow *ui;
    bool saveAndExitFlag;
    QString clientID, deviceID;

    QSocketNotifier *snTerm;
};

static int sigtermFd[2];

#endif // MAINWINDOW_H
