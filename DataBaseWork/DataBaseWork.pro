#-------------------------------------------------
#
# Project created by QtCreator 2016-02-22T22:15:42
#
#-------------------------------------------------

QT       += sql

QT       -= gui

TARGET = DataBaseWork
TEMPLATE = lib

DEFINES += DATABASEWORK_LIBRARY

SOURCES += databasework.cpp

HEADERS += databasework.h\
        databasework_global.h
QMAKE_LFLAGS_RELEASE += -static-libgcc


unix {
    target.path = /usr/lib
    INSTALLS += target
    myheaders.path = /usr/include
    myheaders.files = *.h
    INSTALLS += myheaders
}
