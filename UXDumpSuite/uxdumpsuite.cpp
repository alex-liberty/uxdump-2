#include "uxdumpsuite.h"
#include "ui_uxdumpsuite.h"

UXDumpSuite::UXDumpSuite(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::UXDumpSuite)
{
    ui->setupUi(this);
        isStartCreate = true;
    db = new DataBaseWork();
    db->connectBaseMySQL();

    ui->checkBoxMoreFilter->setVisible( false );
    ui->lineEditMAkeQuery->setVisible( false );
    ui->pushButtonMakeQuery->setVisible( false );

    ui->pushButtonClear->setIcon(QIcon( ":/clear" ) );
    ui->pushButtonSave->setIcon(QIcon( ":/save" ) );
    createAllList();
    DrowPlot();
    connect( dPicker, SIGNAL( appended( const QPoint & ) ),
             SLOT( click_on_canvas( const QPoint & ) ) );

#ifdef Q_OS_LINUX
    //linux code goes here
    slash = "/";

#elif Q_OS_WIN32
    // windows code goes here
    slash = "\"";

#else
#error Platform not supported
#endif
}


UXDumpSuite::~UXDumpSuite()
{
    delete ui;
}
/**
 * @brief UXDumpSuite::startProcess
 * Start proccess, which was selected, and wait wail it would be closed.
 * @param nameProcess Selected process
 */
void UXDumpSuite::startProcess( QString nameProcess )
{
    path = QDir::currentPath() + slash + nameProcess;
    proc = new QProcess();
    proc->start( path );

    if( !proc->waitForFinished( 3000000 ) )
        proc->finished(1);
}

/**
 * @brief UXDumpSuite::createAllList
 * Full data in all ComboBoxes in form
 */
void UXDumpSuite::createAllList()
{
    tests = db->selectTestName();
    softwares = db->selectDistinctSoftwareName();
    logins = db->selectPersonLogin();
    modules = db->selectMeasuringModuleName();
    dateTimes = db->selectDistinctDateTime();
    parameters = db->selectDistinctParameterName();

    tests.append( "" );
    softwares.append( "" );
    logins.append( "" );
    modules.append( "" );
    parameters.append( "" );
    dateTimes.append( "" );

    ui->comboBoxDateTime->clear();
    ui->comboBoxModule->clear();
    ui->comboBoxParameter->clear();
    ui->comboBoxPerson->clear();
    ui->comboBoxSoftware->clear();
    ui->comboBoxTest->clear();

    ui->comboBoxTest->addItems( tests );
    ui->comboBoxSoftware->addItems( softwares );
    ui->comboBoxPerson->addItems( logins );
    ui->comboBoxModule->addItems( modules );
    ui->comboBoxParameter->addItems( parameters );
    ui->comboBoxDateTime->addItems( dateTimes );
}

void UXDumpSuite::on_pushButtonAcquire_clicked()
{
    startProcess( "addTest" );
    createAllList();
}

void UXDumpSuite::on_pushButtonDBManager_clicked()
{
    startProcess( "databaseManager" );
    createAllList();
}

void UXDumpSuite::on_pushButtonMakePlot_clicked()
{
    QString name;
    QString kind;

    pointsBeats.clear();
    intervalValues.clear();
    yParameter.clear();
    isDateTime = false;

    if ( !saveValues.isEmpty() )
        saveValues.removeFirst();

    QString arg1 = ui->comboBoxXAxis->currentText();
    if (arg1 == "Person" ) {
        login = ui->comboBoxYAxis->currentText();
        name = login;
    }

    if (arg1 == "Parameter" ) {
        parameter = ui->comboBoxYAxis->currentText();
        name = parameter;
    }

    if ( arg1 == "Software" ) {
        software = ui->comboBoxYAxis->currentText();
        name = software;
    }

    if ( arg1 == "Test" ) {
        test = ui->comboBoxYAxis->currentText();
        name = test;
    }

    outputValues = db->selectValue( module, test, software, login, parameter, dateTime );
    valuePlot = outputValues[ 0 ];
    yParameter.append( name );
    analysesDateTime( valuePlot[ 0 ] );

    if ( isDateTime )
        handlerDateTime( valuePlot );

    else
        handlerDoubleValues( valuePlot );

    kind = ui->comboBoxKindPlot->currentText();
    if ( kind == "Curve" || "Points" || "Stepped curve" )
        addCurve(name, kind);

    if( kind == "Histogram" )
        addHistogram(name);

    xParameter.clear();
    xParameter.append( "Step" );

    for ( int i = 0; i < yParameter.count() - 1; i++ )
        xParameter << QString::number( i );

    saveValues.prepend( xParameter );
    saveValues << yParameter;
}

/**
 * @brief UXDumpSuite::makeSelect
 * Use function DataBaseWork::selectValue and full data in TableWidgets
 */
void UXDumpSuite::makeSelect()
{
    module = ui->comboBoxModule->currentText();
    test = ui->comboBoxTest->currentText();
    software = ui->comboBoxSoftware->currentText();
    login = ui->comboBoxPerson->currentText();
    parameter = ui->comboBoxParameter->currentText();
    dateTime = ui->comboBoxDateTime->currentText();
    outputValues = db->selectValue( module, test, software, login, parameter, dateTime );

    dataOutput.clear();
     idParameterOutput.clear();
    idLogOutput.clear();

    dataOutput              = outputValues[ 0 ];
    idParameterOutput       = outputValues[ 1 ];
    idLogOutput             = outputValues[ 2 ];

    personLoginOutput       = outputValues[ 3 ];
    personSexOutput         = outputValues[ 4 ];
    personYOBOutput         = outputValues[ 5 ];

    testNameOutput          = outputValues[ 6 ];
    measuringModuleOutput   = outputValues[ 7 ];
    softwareNameOutput      = outputValues[ 8 ];

    logPersonOutput         = outputValues[ 9 ];
    logTestOutput           = outputValues[ 10 ];
    logSoftwareOutput       = outputValues[ 11 ];
    logModuleOutput         = outputValues[ 12 ];
    logDurationOutput       = outputValues[ 13 ];
    logDateTimeOutput       = outputValues[ 14 ];

    parameterNameOutput     = outputValues[ 15 ];

    for( int i = 0; i < ui->tableWidgetTest->rowCount(); i++)
        ui->tableWidgetTest->removeRow( i );

    ui->tableWidgetTest->setRowCount(0);

    for ( int i = 0; i < testNameOutput.count(); i++ ) {
        ui->tableWidgetTest->insertRow( i );
        addItemInTable( ui->tableWidgetTest, testNameOutput[ i ], i, 0 );

    }
    for( int i = 0; i < ui->tableWidgetSoftware->rowCount(); i++)
        ui->tableWidgetSoftware->removeRow( i );

    ui->tableWidgetSoftware->setRowCount(0);

    for ( int i = 0; i < softwareNameOutput.count(); i++ ) {
        ui->tableWidgetSoftware->insertRow( i );
        addItemInTable( ui->tableWidgetSoftware, softwareNameOutput[ i ], i, 0 );
        //addItemInTable( ui->tableWidgetSoftware, softwareTestOutput[ i ], i, 1 );
    }

    for( int i = 0; i < ui->tableWidgetModule->rowCount(); i++)
        ui->tableWidgetModule->removeRow( i );
    ui->tableWidgetModule->setRowCount(0);

    for ( int i = 0; i < measuringModuleOutput.count(); i++ ) {
        ui->tableWidgetModule->insertRow( i );
        addItemInTable( ui->tableWidgetModule, measuringModuleOutput[ i ], i, 0 );
    }

    for( int i = 0; i < ui->tableWidgetPerson->rowCount(); i++)
        ui->tableWidgetPerson->removeRow( i );
    ui->tableWidgetPerson->setRowCount(0);

    for ( int i = 0; i < personLoginOutput.count(); i++ ) {
        ui->tableWidgetPerson->insertRow( i );
        addItemInTable( ui->tableWidgetPerson, personLoginOutput[ i ],  i, 0 );
        addItemInTable( ui->tableWidgetPerson, personSexOutput[ i ],    i, 1 );
        addItemInTable( ui->tableWidgetPerson, personYOBOutput[ i ],    i, 2 );
    }


    for( int i = 0; i < ui->tableWidgetLog->rowCount(); i++)
        ui->tableWidgetLog->removeRow( i );
    ui->tableWidgetLog->setRowCount(0);

    for ( int i = 0; i < logPersonOutput.count(); i++ ) {
        ui->tableWidgetLog->insertRow( i );

        addItemInTable( ui->tableWidgetLog, logPersonOutput[ i ],   i, 0 );
        addItemInTable( ui->tableWidgetLog, logTestOutput[ i ],     i, 1 );
        addItemInTable( ui->tableWidgetLog, logSoftwareOutput[ i ], i, 2 );
        addItemInTable( ui->tableWidgetLog, logModuleOutput[ i ],   i, 3 );
        addItemInTable( ui->tableWidgetLog, logDurationOutput[ i ], i, 4 );
        addItemInTable( ui->tableWidgetLog, logDateTimeOutput[ i ], i, 5 );
    }


    for( int i = 0; i < ui->tableWidgetParameter->rowCount(); i++)
        ui->tableWidgetParameter->removeRow( i );

    ui->tableWidgetParameter->setRowCount(0);

    for ( int i = 0; i < parameterNameOutput.count(); i++ ) {
        ui->tableWidgetParameter->insertRow( i );
        addItemInTable( ui->tableWidgetParameter, parameterNameOutput[ i ],    i, 0 );
        //addItemInTable( ui->tableWidgetParameter, parameterLogOutput[ i ],     i, 1 );
        //addItemInTable( ui->tableWidgetParameter, parameterColumnOutput[ i ],  i, 2 );
    }

    for( int i = 0; i < ui->tableWidgetValues->rowCount(); i++)
        ui->tableWidgetValues->removeRow( i );

    ui->tableWidgetValues->setRowCount(0);

    for ( int i = 0; i < dataOutput.count(); i++ ) {
        ui->tableWidgetValues->insertRow( i );

        addItemInTable( ui->tableWidgetValues,  idParameterOutput[ i ], i, 0 );
        addItemInTable( ui->tableWidgetValues, idLogOutput[ i ],        i, 1 );
        addItemInTable( ui->tableWidgetValues, dataOutput[ i ],         i, 2 );
    }

    updateYaxis();
}

/**
 * @brief UXDumpSuite::addItemInTable
 * Add value in TableWidget
 * @param table TableWidget
 * @param value Value, which nessesary add
 * @param row Number of row in TableWidget
 * @param column Number of column in TableWidget
 */
void UXDumpSuite::addItemInTable( QTableWidget *table, QString value, int row, int column )
{
    item = new QTableWidgetItem;
    item->setText( value );
    item->setTextAlignment( Qt::AlignCenter );
    table->setItem( row, column, item );
}

void UXDumpSuite::on_comboBoxTest_currentTextChanged(const QString &arg1)
{
    if ( !isStartCreate )
        makeSelect();
}

void UXDumpSuite::on_comboBoxSoftware_currentTextChanged(const QString &arg1)
{
    if ( !isStartCreate )
        makeSelect();
}

void UXDumpSuite::on_comboBoxModule_currentTextChanged(const QString &arg1)
{
    if ( !isStartCreate )
        makeSelect();
}

void UXDumpSuite::on_comboBoxParameter_currentTextChanged(const QString &arg1)
{
    if ( !isStartCreate )
        makeSelect();
}

void UXDumpSuite::on_comboBoxPerson_currentTextChanged(const QString &arg1)
{

    if ( !isStartCreate )
        makeSelect();
}

void UXDumpSuite::on_comboBoxDateTime_currentTextChanged(const QString &arg1)
{
    isStartCreate = false;
    makeSelect();
}

void UXDumpSuite::on_comboBoxXAxis_currentTextChanged(const QString &arg1)
{
    updateYaxis();
}

/**
 * @brief UXDumpSuite::updateYaxis
 * Full data in ComboBox "YAxis"
 */
void UXDumpSuite::updateYaxis()
{
    QString arg1 = ui->comboBoxXAxis->currentText();
    ui->comboBoxYAxis->clear();

    if ( arg1 == "Person" )
        ui->comboBoxYAxis->addItems( personLoginOutput );

    if ( arg1 == "Parameter" )
        ui->comboBoxYAxis->addItems( parameterNameOutput );

    if( arg1 == "Software" )
        ui->comboBoxYAxis->addItems( softwareNameOutput );

    if ( arg1 == "Test" )
        ui->comboBoxYAxis->addItems( testNameOutput );

    if ( arg1 == "Measuring module" )
        ui->comboBoxYAxis->addItems( measuringModuleOutput );
}

/**
 * @brief UXDumpSuite::DrowPlot
 * Drow plot and create it's settings
 */
void UXDumpSuite::DrowPlot()
{
    ui->qwtPlot->setAxisTitle( QwtPlot::yLeft, "Value" );
    ui->qwtPlot->setAxisTitle( QwtPlot::xBottom, "Time stamp" );
    ui->qwtPlot->setAxisLabelAlignment( QwtPlot::xBottom, Qt::AlignLeft | Qt::AlignBottom );

    QwtPlotGrid *grid = new QwtPlotGrid();
    grid->setMajorPen( QPen( Qt::gray, 2 ) );
    grid->attach( ui->qwtPlot );

    QwtPlotMagnifier *magnifier = new QwtPlotMagnifier( ui->qwtPlot->canvas() );
    magnifier->setMouseButton( Qt::MidButton );

    QwtPlotPanner *dPanner = new QwtPlotPanner( ui->qwtPlot->canvas() );
    dPanner->setMouseButton( Qt::RightButton );

    dPicker = new QwtPlotPicker(
                QwtPlot::xBottom, QwtPlot::yLeft, QwtPlotPicker::CrossRubberBand,
                QwtPicker::ActiveOnly, ui->qwtPlot->canvas() );

    dPicker->setRubberBandPen( QColor( Qt::red ) );
    dPicker->setTrackerPen( QColor( Qt::black ) );
    dPicker->setStateMachine( new QwtPickerDragPointMachine() );

    ui->qwtPlot->replot();
}

/**
 * @brief UXDumpSuite::addCurve
 * Add selected kind of curve
 * @param name
 * @param kind
 */
void UXDumpSuite::addCurve(QString name, QString kind)
{
    static const Qt::GlobalColor colors[] = { Qt::white, Qt::black, Qt::red, Qt::darkRed,
                                              Qt::green, Qt::darkGreen, Qt::darkMagenta,
                                              Qt::darkRed,Qt::darkYellow, Qt::darkGray };

    int idx = rand() % ( sizeof( colors ) / sizeof( *colors ) );
    int thickness;
    curve = new QwtPlotCurve( name );
    QColor rndColor = QColor( colors[ idx ] );

    createLegent();

    if ( kind == "Points" ) {
        curve->setStyle( QwtPlotCurve::Dots );
        thickness = 5;
    }

    if ( kind == "Curve" ) {
        curve->setStyle( QwtPlotCurve::Lines );
        thickness = 2;
    }

    if ( kind == "Stepped curve" ) {
        curve->setStyle( QwtPlotCurve::Steps );
        thickness = 2;
    }

    if ( kind == "Fairy lights" ) {
        curve->setStyle( QwtPlotCurve::Lines );
        thickness = 2;
        QwtSymbol *symbol = new QwtSymbol( QwtSymbol::Ellipse,
                                           QBrush( Qt::yellow ), QPen( Qt::red, 2 ),
                                           QSize( 8, 8 ) );
        curve->setSymbol( symbol );
    }

    curve->setPen( QPen( rndColor, thickness ) );
    curve->setRenderHint( QwtPlotItem::RenderAntialiased, true );
    curve->setSamples( pointsBeats );
    curve->attach( ui->qwtPlot );

    ui->qwtPlot->replot();
}

void UXDumpSuite::addHistogram(QString name)
{
    static const Qt::GlobalColor colors[] = { Qt::white, Qt::black, Qt::red, Qt::darkRed,
                                              Qt::green, Qt::darkGreen, Qt::darkMagenta,
                                              Qt::darkRed,Qt::darkYellow, Qt::darkGray };

    int idx = rand() % ( sizeof( colors ) / sizeof( *colors ) );
    histogram = new QwtPlotHistogram( name );
    QColor rndColor = QColor( colors[ idx ] );

    createLegent();

    histogram->setStyle( QwtPlotHistogram::Columns );

    histogram->setBrush( rndColor );
    histogram->setSamples( intervalValues );
    histogram->attach( ui->qwtPlot );

    ui->qwtPlot->replot();
}

void UXDumpSuite::createLegent()
{
    QwtLegend *leg;
    leg = new QwtLegend();
    ui->qwtPlot->insertLegend( leg,QwtPlot::RightLegend );
}

void UXDumpSuite::on_pushButtonClear_clicked()
{
    xParameter.clear();
    saveValues.clear();
    ui->qwtPlot->detachItems();
    DrowPlot();
    ui->qwtPlot->replot();
}

void UXDumpSuite::on_pushButtonSave_clicked()
{
    saveFile();
}

void UXDumpSuite::saveFile()
{
    QwtPlotRenderer renderer;
    QString fileName = QFileDialog::getSaveFileName( this, tr( "Save File" ),
                                                     "untitled.png",
                                                     tr( "*.png;; *.xpm;; *.jpg;; *.jpeg;; *.svg;;Text files (*.csv)" ) );
    QFile file( fileName );
    QFileInfo *info = new QFileInfo( file );
    ui->qwtPlot->replot();

    if ( info->completeSuffix() != "csv" )
        renderer.renderDocument( ui->qwtPlot, fileName, QSizeF( 1000, 500 ) );

    else {
        QString saveString;
        int column;
        QFile file( fileName + ".csv" );
        if ( file.open( QIODevice::Append ) ) {
            for ( int i = 0; i < xParameter.count(); i++ ){
                column = 1;

                foreach ( QStringList value, saveValues ) {
                    saveString += value[ i ];
                    if ( column != saveValues.count() )
                        saveString += ",";
                    else
                        saveString += "\n";
                    column++;

}            }

            QTextStream stream( &file );
            stream << saveString;
            file.close();
        }
    }
}

void UXDumpSuite::analysesDateTime( QString value )
{
    QStringList firstSeparator = value.split( " " );

    if ( firstSeparator.count() > 1 ) {
        QStringList secondSeparator = firstSeparator[ 0 ].split( "-" );
        QStringList thirdSeparator  = firstSeparator[ 1 ].split( ":" );

        if ( secondSeparator.count() == 3 && thirdSeparator.count() == 4 ) {
            dateTimeFormat = "yyyy-MM-dd hh:mm:ss:zzz";
            isDateTime = true;
        }
    }

    else {
        firstSeparator = value.split( "-" );

        if ( firstSeparator.count() == 3 ) {
            dateTimeFormat = "yyyy-MM-dd";
            isDateTime = true;
        }
        else {
            firstSeparator = value.split( ":" );

            if ( firstSeparator.count() == 4) {
                dateTimeFormat = "hh:mm:ss:zzz";
                isDateTime = true;
            }
        }
    }
}

void UXDumpSuite::handlerDateTime(QStringList values)
{
    int first;
    int step = 0;
    QDateTime now = QDateTime::currentDateTime();
    for( double i = 0; i < values.count(); i++ ) {
        QDateTime dateValue = QDateTime::fromString( values[ i ], dateTimeFormat );
        int value = dateValue.msecsTo( now );
        valuePoint << value;
        pointsBeats << QPointF( i, value );
        yParameter << QString::number(value);
        first = step++;
        intervalValues << QwtIntervalSample( value, first, step );
    }
}

void UXDumpSuite::handlerDoubleValues(QStringList values)
{
    int first;
    int step = 0;
    for( double i = 0.00; i < values.count(); i++ ) {
        valuePoint << values[ i ].toDouble();
        yParameter << values[ i ];
        pointsBeats << QPointF( i, values[i].toDouble() );

        first = step++;
        intervalValues << QwtIntervalSample( values[ i ].toDouble(), first, step );
    }
}

void::UXDumpSuite::click_on_canvas(const QPoint &pos)
{
    double x = ui->qwtPlot->invTransform( QwtPlot::xBottom, pos.x() );
    double y = ui->qwtPlot->invTransform( QwtPlot::yLeft, pos.y() );

    QString infoX = "Time (click at " + QString::number( x )+ " )";
    QString infoY = "Value (click at " + QString::number( y ) + " )";

    ui->qwtPlot->setAxisTitle( QwtPlot::yLeft, infoY );
    ui->qwtPlot->setAxisTitle( QwtPlot::xBottom, infoX );
}
