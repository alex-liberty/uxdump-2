#ifndef UXDUMPSUITE_H
#define UXDUMPSUITE_H

#include <QMainWindow>
#include <QProcess>
#include <QDir>
#include <QApplication>
#include <QPixmap>
#include <QSplashScreen>
#include <QWidget>
#include <QTimer>
#include <QThread>
#include <QFileDialog>
#include <QFile>
#include <QTableWidgetItem>
#include <QDebug>

#include <databasework.h>

#include "qwt_plot.h"
#include <qwt_plot_histogram.h>
#include <qwt_plot_curve.h>
#include <qwt_plot_legenditem.h>
#include <qwt_legend.h>
#include <qwt_plot_grid.h>
#include <qwt_plot_magnifier.h>
#include <qwt_plot_panner.h>
#include <qwt_plot_picker.h>
#include <qwt_plot_renderer.h>
#include <qwt_picker_machine.h>
#include <qwt_symbol.h>

namespace Ui {
class UXDumpSuite;
}

class UXDumpSuite : public QMainWindow
{
    Q_OBJECT

public:
    explicit UXDumpSuite(QWidget *parent = 0);
    ~UXDumpSuite();

private slots:
    void on_pushButtonAcquire_clicked();

    void on_pushButtonDBManager_clicked();

    void on_pushButtonMakePlot_clicked();

    void on_comboBoxTest_currentTextChanged(const QString &arg1);

    void on_comboBoxSoftware_currentTextChanged(const QString &arg1);

    void on_comboBoxModule_currentTextChanged(const QString &arg1);

    void on_comboBoxParameter_currentTextChanged(const QString &arg1);

    void on_comboBoxPerson_currentTextChanged(const QString &arg1);

    void on_comboBoxDateTime_currentTextChanged(const QString &arg1);

    void on_comboBoxXAxis_currentTextChanged(const QString &arg1);

    void on_pushButtonClear_clicked();

    void on_pushButtonSave_clicked();

    void click_on_canvas( const QPoint &pos );


private:
    Ui::UXDumpSuite *ui;

    void startProcess(QString nameProcess );
    void createAllList();
    void makeSelect();
    void updateYaxis();
    void DrowPlot();
    void addCurve(QString name, QString kind);
    void addHistogram(QString name);
    void saveFile();
    void analysesDateTime(QString value);
    void handlerDateTime( QStringList values );
    void handlerDoubleValues(QStringList values);
    void addItemInTable(QTableWidget *table, QString value, int row, int column );
    void createLegent();

    QString path;
    QString slash;
    QProcess *proc;
    DataBaseWork *db;

    QStringList tests;
    QStringList softwares;
    QStringList logins;
    QStringList modules;
    QStringList dateTimes;
    QStringList parameters;
    QStringList values;

    QList<QStringList> outputValues;

    QString module;
    QString test;
    QString software;
    QString login;
    QString parameter;
    QString dateTime;

    QString dateTimeFormat;

    QStringList dataOutput;
    QStringList idParameterOutput;
    QStringList idLogOutput;
    QStringList valueParameterNameOutput;
    QStringList valueParameterColumnOutput;
    QStringList personLoginOutput;
    QStringList personSexOutput;
    QStringList personYOBOutput;
    QStringList testNameOutput;
    QStringList measuringModuleOutput;
    QStringList softwareNameOutput;
    QStringList softwareTestOutput;
    QStringList logPersonOutput;
    QStringList logTestOutput;
    QStringList logSoftwareOutput;
    QStringList logModuleOutput;
    QStringList logDurationOutput;
    QStringList logDateTimeOutput;
    QStringList parameterNameOutput;
    QStringList parameterLogOutput;
    QStringList parameterColumnOutput;
    QStringList valuePlot;

    QList <QStringList> saveValues;
    QStringList xParameter;
    QStringList yParameter;

    QVector<QwtIntervalSample> intervalValues;
    QwtPlotHistogram *histogram;
    QwtPlotCurve *curve;
    QwtPlotPicker *dPicker;

    QPolygonF pointsBeats;
    QList<double> valuePoint;
    QList<double> timePoint;

    QTableWidgetItem* item;

    bool isDateTime;
    bool isStartCreate;

};

#endif // UXDUMPSUITE_H
